﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_227_Milestone_1
{
    //William Thornton, Milestone 1, 07/23/2019, driver class
    //I'm unable to use private classes because they are in a namespace. Any advice on this would be appreciated.
    class Program
    {
        static void Main(string[] args)
        {
            GameBoard game = new GameBoard(10);
            game.Activate();
            game.FindNeighbors();
            game.revealGrid();

        }
    }
}
